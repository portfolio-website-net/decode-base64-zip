﻿using System;
using System.IO;
using System.IO.Compression;

namespace decode_base64_zip
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var inputFilePath = args[0];
            var zipFileName = inputFilePath.Replace(".base64", string.Empty);

            var fileContent = string.Join("\n", System.IO.File.ReadAllLines(inputFilePath));
            var bytes = Convert.FromBase64String(fileContent);
            using (var source = new MemoryStream(bytes))
            {                
                using (var destination = new FileStream(zipFileName, FileMode.Create))
                { 
                    using (var bw = new BinaryWriter(destination))
                    {
                        byte[] buffer = new byte[2048];
                        int bytesRead;
                        while((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0) 
                        {
                            destination.Write(buffer, 0, bytesRead);
                        }
                    }
                }
            }
        }
    }
}
